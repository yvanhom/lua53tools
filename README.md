# Lua 5.3.4 32-bit binary tools for windows

## Include

From [is73/lua53](https://github.com/is73/lua53):

* [lua](https://www.lua.org/) 5.3.3
* [lpeg](http://www.inf.puc-rio.br/~roberto/lpeg/lpeg.html) 1.0.0-1
* [lrexlib-pcre](http://rrthomas.github.io/lrexlib/) 2.8.0-1
* [lsqlite3](http://lua.sqlite.org/index.cgi/index) 0.9.3-0
* [luafilesystem](http://keplerproject.github.io/luafilesystem/) 1.6.3-2
* [luasec](https://github.com/brunoos/luasec) 0.6-1
* [luasocket](http://w3.impa.br/~diego/software/luasocket/) 3.0rc1-2
* [luasql-mysql](https://github.com/keplerproject/luasql) cvs-1
* [luautf8](https://github.com/starwing/luautf8) 0.1.1-1
* [md5](https://github.com/keplerproject/md5) 1.2-1
* [microlight](http://stevedonovan.github.io/microlight/) 1.0-1
* [penlight](http://stevedonovan.github.io/Penlight/api/index.html) 1.3.2-2

## Add

* [luazen](https://github.com/philanc/luazen) 0.7-1
* [lua-cjson](https://www.kyne.com.au/~mark/software/lua-cjson.php) 2.1devel
* [lua-iconv](http://luaforge.net/projects/lua-iconv/) 7 (Last update since 2012)
* [lua-htmlparser](https://github.com/msva/lua-htmlparser) v0.3.3
* [wsapi](http://keplerproject.github.io/wsapi/) git-20160705
* [coxpcall](http://keplerproject.github.io/coxpcall/) git-20160223
* [copax](http://keplerproject.github.io/copas) git-20161001
* [cgilua](http://keplerproject.github.com/cgilua) git-20160612
* [xavante](http://keplerproject.github.com/xavante) git-20161015
* [rings](http://keplerproject.github.com/rings) git-20170405
* [luaduk](https://github.com/padicao2010/luaduk.git)
* [30log](https://github.com/Yonaba/30log) 1.3.0-1

## Update

* [lua](https://www.lua.org/) 5.3.4
* [penlight](http://stevedonovan.github.io/Penlight) git-20170115
